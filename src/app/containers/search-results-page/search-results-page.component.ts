import { Component, OnInit, Input } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { ActivatedRoute, Router, ChildActivationEnd } from "@angular/router";

import * as fromSearch from '../../core/reducers/selectors/index';
import { LoadSearches, LoadFilmDetails } from '../../core/actions/search.actions';

import { filmInfo } from '../../core/models/film-info';
import { ModelBuilderService } from '../../core/services/modular-buider.service';


@Component({
  selector: 'app-search-results-page',
  templateUrl: './search-results-page.component.html',
  styleUrls: ['./search-results-page.component.css']
})
export class SearchResultsPageComponent implements OnInit {

  results: any[];
  query: string;
  searchType: string;
  pageable: any;
  queryData: any;
  executefilmCall: boolean;
  browserRefresh: boolean;

  @Input("record") record: any
  @Input("searchType") searchType: string

  filminfo: filmInfo

  constructor(private store: Store<fromSearch.State>,
    private router: Router,
    private modelBuilder: ModelBuilderService,
    private activedRoute: ActivatedRoute) {
    this.router.events.subscribe((event) => {
      if (event instanceof ChildActivationEnd) {
        this.browserRefresh = !router.navigated;
      }
    });
  }

  results$ = this.store.pipe(select(fromSearch.getResults));

  ngOnInit() {
    this.retrieveSearchData();
    this.buildResultsSet();
  }

  ngOnChange(){
    this.retrieveSearchData()
  }

  retrieveSearchData() {
    if (this.browserRefresh || this.results == undefined) {
      this.query = this.activedRoute.snapshot.paramMap.get("query")
      this.searchType = this.activedRoute.snapshot.paramMap.get("type")
      this.store.dispatch(new LoadSearches({ query: this.query, searchType: this.searchType.toUpperCase(), page: 1 }))
    }

    this.results$.subscribe((data: any) => {
      this.queryData = data["queryData"];
      if(this.queryData != undefined){
        this.query =  this.queryData.query
        let st = this.queryData.searchType != null && this.queryData.searchType != undefined ? this.queryData.searchType.toLowerCase() : ""
        this.searchType = st.length > 0 ? st.charAt(0).toUpperCase() + st.slice(1) : ""
      }
      this.results = data["results"];
      this.executefilmCall = data["executedFilmCall"];
      this.pageable = data['pageable'];
    });
  }

  buildResultsSet(){
    switch(this.searchType.toUpperCase()){
      case "FILM": this.filminfo = this.modelBuilder.buildFilm(this.record)
        break;
     default:
       break;
    }
  }

  viewDetails(){
    switch(this.searchType.toUpperCase()){
      case "PRODUCTS": 
          this.store.dispatch(new LoadFilmDetails(this.filminfo.url));
          this.router.navigate([`results/film`,  this.filminfo.episodeId]);
        break;
      default:
        break;
    }
  }

}
