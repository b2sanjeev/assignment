
import { Action} from 'ngrx/store';

export enum FilmActionTypes {
    LoadFilmDetails = '[film] Load Film Details';
    LoadFilmDetailsSuccess = '[film/API] Load Film Details Success';
    LoadFilmDetailsFailure = '[film/API] Load Film Details failure';
}

export class LoadFilmDetails implements Action {
    readonly type = FilmActionTypes.LoadFilmDetails;
    constructor(public payload:string){}
}
export class LoadFilmDetailsSuccess implements Action {
    readonly type = FilmActionTypes.LoadFilmDetailsSuccess;
    constructor(public payload:string){}
}
export class LoadFilmDetailsFailure implements Action {
    readonly type = FilmActionTypes.LoadFilmDetailsFailure;
    constructor(public payload:string){}
}

export type FilmActions = LoadFilmDetails | LoadFilmDetailsSuccess | LoadFilmDetailsFailure