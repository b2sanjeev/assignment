import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  type ="FILMS"

  constructor(private router: Router, private activedRoute){}

  ngOnInit(){
    let searchType = this.activedRoute.snapshot.paramMap.get("type");
    let url = this.activedRoute.url;
    if(searchType != undefined){
      this.type = searchType.toUpperCase()
    }
  }
}

