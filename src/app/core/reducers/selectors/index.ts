import * as fromSearch from '../search.reducer';
import * as fromRoot from '../root.reducer';
import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';
import { SearchActions } from '../../actions/search.actions';

export interface SearchState {
    searchPage: fromSearch.state;
}

export interface State extends fromRoot.State {
    search: SearchState;
}

export const reducers: ActionReducerMap<SearchState, SearchActions> = {
    searchPage: fromSearch.reducer
}

export const