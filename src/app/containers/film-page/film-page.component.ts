import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import * as fromSearch from '../../search/reducers';
import { filmInfo } from '../../core/models/film-info';
import {Pipe, PipeTransform} from '@angular/core';

@Component({
  selector: 'app-film-page',
  templateUrl: './film-page.component.html',
  styleUrls: ['./film-page.component.css']
})
export class FilmPageComponent implements OnInit {

  constructor(private store: Store<fromSearch.State>) { }
  
  filmDetails$ = this.store.pipe(select(fromSearch.getFilmDetails));

  @Input("record") record: any

  ngOnInit() {
  }
  
}
