import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchService } from '../services/search.service';
import { DefaultValuePipe } from '../pipes/default-value.pipd';

@NgModule({
    imports:[
        CommonModule
    ],
    declarations: [DefaultValuePipe],
    exports: [DefaultValuePipe],
    providers: [SearchService]
})
export class CoreModule {}