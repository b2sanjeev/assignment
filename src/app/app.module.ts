
// module
import {NgModule} from '@angular/core';
import{ BrowserModule } from '@angular/platform-browser';
import { CommonModule} from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { CoreModule } from './core/modules/core.module';
import { SearchModule } from './core/modules/search.module'
import {HttpClientModule } from '@angular/common/http';

import { reducers, metaReducers } from './core/reducers/root.reducer';
import { EffectModule } from 'ngrx/effects';
import { environment } from '../environments/environment';
import { PaginatorModule } from 'primeeng/paginator';

//component
import { AppComponent } from './app.component';
import { homeComponent} from './Home/home.component';
import { notFoundComponent} from './noFound.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './component/footer/footer.component';
import { SearchComponent } from './component/search/search.component';
import { filmInfoCoimponent } from './component/film-info/film-info.component';
import { appRoutingModule } from './app-routing.module';


@NgModule({
  declarations: [
    AppComponent,
    homeComponent,
    notFoundComponent,
    HeaderComponent,
    FooterComponent,
    filmInfoCoimponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    appRoutingModule,
    CommonModule,
    CoreModule,
    SearchModule,
    HttpClientModule,
    PaginatorModule,
    EffectModule.forRoot([]),
    StoreModule.forRoot(reducers, {metaReducers}),
    !environment.production? StoreDevtoolsModule.instrument(): []
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

