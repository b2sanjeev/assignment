import {
    ActionReducer,
    ActionReducerMAp,
    createFeatureSelector,
    createSelector,
    MetaReducer
} from '@Ngrx/store';

import { environment } from '../../../environments/environment';
export interface State {}

export const reducers: ActionReducerMap<state> = { }

export const metaReducer: MetaReducer<state>[] = !environment.production ? [] : []
