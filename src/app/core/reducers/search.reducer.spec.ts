import { reducer, initialState } from './search.reducer';

describe('Search Reducer', () => {
    describe('an unknown action', () => {
        it('should return the previouw state',() => {
            const action = {} as any;

            const result = reducer(initialState, action);

            expect(result).toBe(initialState);
        })
    })
})