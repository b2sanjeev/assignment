import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@Ngrx/store';
import { EffectsModule } from  '@Ngrx/effects';
import { SearchEffects } from '../effects/search.effects';
import { reducers } from '../reducers/selectors/index';

@NgModule({
    imports: [
        CommonModule,
        StoreModule.forFeature('search', reducers),
        EffectsModule.forFeature([SearchEffects])
    ],
    declarations:[]
})

export class SearchModule {}