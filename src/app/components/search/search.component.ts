import { Component, OnInit, HostListener, Input } from '@angular/core';
import { Router, ActivateRoute, ActivatedRoute } from '@angular/router';
import { Store, Select } from '@ngrx/store';

import * as fromSearch from '../../core/reducers/selectors/index';
import {LoadSearches, SuggestSearches, SearchActionTypes} from '../../core/actions/search.actions';

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./Search.component.css']
})

export class SearchComponent implements OnInit {
    @Input("detailsType") detailsType: string
    query: string;
    searchType: string = "FILMS";
    suggestions: any[];

    constructor(private router: Router, private store: Store<fromSearch.SearchState>,
        private activateRoute: ActivatedRoute
        ){}
    
    ngOnInit(){
        let type = this.detailsType == undefined? this.activateRoute.snapshot.paramMap.get("type") : this.detailsType;
        if(type != undefined && type != null){
            this.searchType=type.toUpperCase()
        }
        this.retrieveSuggestions();
        if(this.query == null || this.query == undefined || this.query.trim() == ""){
            this.suggestions = undefined
        }
    }

    retrieveSuggestions(){
        this.suggestions$.subscribe((data: any) =>{
            this.suggestions = data
        })
    }

    @HostListener('document:keyup', ['$event'])
    Search(event: KeyboardEvent){
        if((event.type == "keyup" && event.type == undefined) || event.key.toLowerCase()== 'enter')){
            this.store.dispatch(new LoadSearches({query: this.query, searchType: this.searchType.toUpperCase(), page: 1}));
            this.router.navigate([`search`, this.searchType.toLowerCase(), this.query]);
            this.suggestions = undefined;
        }else {
            if(this.query != undefined && this.query != null && this.query.trim()!= "" && this.query.length >=3){
                this.store.dispatch(new SuggestSearches({query: this.query, searchType: this.searchType.toUpperCase(), page: 1}));
            } else {
                this.suggestions = undefined;
            }
        }
    }

    @HostListener('document:click', ['$event']) clickedOutside($event){
        if(this.query == null || this.query == undefined || this.query.trim() == ""){
            this.suggestions = undefined
        }
    }

    searchData(searchQuery: any) {
        this.store.dispatch(new LoadSearches(new LoadSearches({query: this.query, searchType: this.searchType.toUpperCase(), page: 1}));
        this.clear();
        this.router.navigate([`search`, this.searchType.toLowerCase(), this.query]);
    }

    clear(){
        this.query = '';
        this.suggestions = undefined;
    }

}