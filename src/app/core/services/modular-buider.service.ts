import { Injectable } from '@angular/core';
import { filmInfo } from '../models/film-info';

@Injectable({
    providedIn: 'root'
})

export class ModelBuilderService {
    constructor(){}

    buildFilm(record): filmInfo{
        if(this.recordIsValid(record)){
            let filmRecord = new FilmInfo();
            filmRecord.title = record["title"];
            filmRecord.episodeId = record["episodeId"];
            filmRecord.openingCrawl = record["openingCrawl"];
            filmRecord.director = record["director"];
            filmRecord.producer = record["producer"];
            filmRecord.releaseDate = record["releaseDate"];
            filmRecord.created = record["created"];
            filmRecord.edited = record["edited"];
            filmRecord.url = record["url"];

        }
    }

    recordIsValid(record): Boolean {
        return record != null && record != undefined
    }
}