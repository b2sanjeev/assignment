import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';


import {Imusic} from '../Interfaces/music.model';

@Injectable()

export class musicService{
        
    constructor (private _http :HttpClient) {};
    private _musicUrl = 'https://ngmusicdb.herokuapp.com/api/getMusic';

    getMusic(): Observable<Imusic[]>{
        return this._http.get<Imusic[]>(this._musicUrl);
    }

    getAlbum(id): Observable<Imusic[]>{
        return this._http.get<Imusic[]>(`${this._musicUrl}?id=${id}`)
    }

    
}


