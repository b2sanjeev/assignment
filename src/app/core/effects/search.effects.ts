import {SearchService} from './../../core/services/search.service';
import { Injectable } from '@angular/core';
import { Actions, Effect, ofType} from '@ngrx/effects';

import {concatMap, map, switchMap, catchError} from 'rxjs/operator';
import {EMPTY, of, Observable, from} from 'rxjs';
import { SearchActionTypes, SearchActions, LoadSearches, LoadFilmDetails, SearchSuccess, SearchFailure, FilmDetailsSuccess, FilmDetailsFailure, PersistDetails} from '../actions/search.actions';


@Injectable()
export class SearchEffects{
    @Effect()
    LoadSearches$ = this.actions$.pipe(
        ofType(SearchActionTypes.LoadSearches),
        map(action => (action as any).payload),
        switchMap(queryData => {
            return from(this.seachService.getConditnalDataUsingAsync(queryData)).pipe(
                map((data: any) => {
                    new SearchSuccess(data.content)
                }),
                catchError(err => {
                   return of(new SearchFailure(err))
                })
            );
        })
    );

    @Effect()
    filmDetails$ = this.actions$.pipe(
        ofType(SearchActionTypes.LoadFilmDetails),
        map(action => (action as any).payload),
        switchMap(url => {
            // Search for Film URL
            return this.serchService.getfilmDetails(url).pipe(
                map((data: any) =>{
                    //film data Found
                    return new FilmDetailsSuccess(data)
                }),
                catError(err => {
                    // Film not Found
                    return of(new LoadFilmDetailsFailure(err))
                })
            );
        })
    );
    constructor(private actions$: Actions<searchActions>, 
        private searchService: SearchService){}
}