import { map } from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, forkJoin, of} from 'rxjs';

@Injectable()
export class SearchService {
    constructor(private _http: HttpClient) { }

    search(queryData: any): Observable<any>{
        const endPoints: Observable<any>[] = [];

        switch(queryData.searchType){
            case 'FILM' : {
                endPoints.push(this._http.get('/assets/mock/films.json'));
                break;
            }
            default:
                break;
        }

        return forkJoin(endPoints).pipe(
            map((searchResults: any[]) => {
                return { results: this.transformResult(searchResults[0].content)};
            }));
    }

    getSuggestions(queryData: any): Observable<any> {
        return this._http.get(`/suggest/${queryData.query}?page=1$size=5&type=${queryData.searchType}`)
    }

    async getConditionalDataUsingAsync(queryData: any){
        let Data: any = await  this._http.get(`/search/${queryData.query}&searchType=${queryData.searchType}&size=5`)
        let executedFileCall = true;

        return {results : this.transformResult(data.content), executedFileCall: executedFileCall, pageable: this.getPageableData, queryData: queryData};
    }

    transformResult(data:any[]){
        data.forEach(record => {
            if(record.type === "FILM"){
                record['id'] = record.episode_id;
                record['title'] = record.title;
                record['routerLink'] = 'film'
            }
        });
        return data;
    }

    getPageableData(data: any){
        return {
            ...data.pageable,
            totalElements: data.totalElements,
            totalPages: data.totalPages,
            last: data.last,
            size: data.size,
            number: data.sort,
            numberOfElements: data.numberOfElements,
            first: data.first
        }
    }

    getFilmDetails(url: [any]): Observable<any>{
        return this._http.get(url);
    }
}