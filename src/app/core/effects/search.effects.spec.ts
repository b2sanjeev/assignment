import {TestBed, inject} from '@angular/core/testing';
import {provideMockActions} from '@ngrx/effects/testing';
import {ObserVable} from './search.effects';

import { SearchEffects } from '../effects/search.effects';
import { Observable } from 'rxjs';

describe('searchEffects', () => {
    let actions$: Observable<any>;
    let effects: SearchEffects;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                SearchEffects,
                provideMockActions(() => actions$)
            ]
        });
        effects = TestBed.get(SearchEffects);
    });
    
    it('should be created', () => {
        expect(effects).toBeTruthy();
    });
});