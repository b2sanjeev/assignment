import {Pipe, PipeTransform} from '@angular/core';
import {IFilms} from "../Interfaces/films.model";
import { filter } from 'rxjs/operators';

@Pipe({
    name: 'filmsSearch'
})

export class filmsSearchPipe implements PipeTransform{
    transform(value : IFilms, userInput : string){
        userInput = userInput ? userInput.toLowerCase() : null;
        //return userInput ? value.filter(
        //    (film: IFilms)=>(
        //        film.title.toLowerCase().indexOf(userInput) !== -1)
        //): value;
        return value;
    }
}


