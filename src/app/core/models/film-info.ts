export class filmInfo {
    title: string;
    episodeId: Number;
    openingCrawl: String;
    director: String;
    producer: string;
    releaseDate: String;
    created: String;
    edited:string;
    url: String;
}