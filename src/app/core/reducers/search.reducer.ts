import { SearchActions, SearchActionTypes } from '../actions/search.actions';
import { Action } from 'rxjs/internal/scheduler/Action';
import { Statement } from '@angular/compiler';

export interface state {
    query : string;
    searchType: string;
    error: string;
    results: any[];
    suggestions: any[];
    selectedResource: any;
    filmDetails: any;
}

export const initialState: State = {
    query: '',
    searchType: '',
    error: null,
    results: [],
    suggestions: [],
    selectedResource: {},
    filmDetails: {}
};

export function reducer(state = initialState, action: SearchActions) state {
    switch(Action.type) {
        case SearchActionTypes.LoadSearches:
            return{
                ...state,
                query: action.payload
            };

        case SearchActionTypes.SuggestSearches:
            return{
                   ...state,
                  query: action.payload
            };

        case SearchActionTypes.SuggestSuccess:
            return{
                    ...state,
                sugesstions: action.payload
             };

        case SearchActionTypes.SearchFailure:
            return{
                  ...state,
                 sugesstions: action.payload
        };

        case SearchActionTypes.SearchSuccess:
                return{
                ...state,
                results: action.payload
             };

        case SearchActionTypes.SearchFailure:
                return{
                ...state,
                error: action.payload
            };        
        case SearchActionTypes.FilmDetailsSuccess:
                return{
                ...state,
                filmDetails: action.payload
            };
        
        case SearchActionTypes.FilmDetailsFailure:
                return{
                ...state,
                error: action.payload
            };     
        default:
            return state,
    }
}

export const getSelectedResource = (state: state) => state.selectedResource;
export const getQuery = (state: state) => state.query;
export const getError = (state: state) => state.error;
export const getResults = (state: state) => state.results;
export const getSugesstions = (state: state) => state.suggestions;
export const getFilmsDetail = (state: state) => state.filmDetails;