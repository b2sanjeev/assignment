import {NgModule} from '@angular/core';
import { Routes, RouterModule} from '@angular/router';
import {filmsComponent} from './films/films.component';
//import {filmsDetailComponent} from './fimls/fimlsDetail/filmsDetail.component';
import { homeComponent } from './Home/home.component';
import { notFoundComponent } from './noFound.component';

const routes: Routes =[
    {path: 'home', component: homeComponent},
    {path: 'films', component: filmsComponent},
   // {path: 'films/:id', component: filmDetailComponent},
    {path: '', redirectTo: 'home', pathMatch: 'full'},
    {path: '**', component: notFoundComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class appRoutingModule{

}