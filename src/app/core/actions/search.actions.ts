import { Action} from 'ngrx/store';

export enum SearchActionTypes {
    LoadSearches = '[Search] Load Searchs',
    SuggestSearches = '[Search] Suggest Searchs',
    LoadFilmDetails = '[Search Result] Load Film Details',
    SearchSuccess = '[Search/API] Search Success',
    SearchFailure = '[Search/API] Search Failure',
    SuggestSuccess = '[Search/API] Search Success',
    SuggestFailure = '[Search/API] Search Failure',
    FilmDetailsSuccess = '[Search/API] Search Success',
    FilmDetailsFailure = '[Search/API] Search Failure',
    PersistDetails = '[Search Result] Save Selected Resource Details',
}

export class LoadSearches implements Action{
    readonly type = SearchActionTypes.LoadSearches;
    constructor (public payload: any){}
}

export class SuggestSearches implements Action{
    readonly type = SearchActionTypes.SuggestSearches;
    constructor (public payload: any){} 
}

export class LoadFilmDetails implements Action{
    readonly type = SearchActionTypes.LoadFilmDetails;
    constructor (public payload: any){}
}

export class SearchSuccess implements Action{
    readonly type = SearchActionTypes.SearchSuccess;
    constructor (public payload: any){}
}

export class SearchFailure implements Action{
    readonly type = SearchActionTypes.SearchFailure;
    constructor (public payload: any){}
}

export class SuggestSuccess implements Action{
    readonly type = SearchActionTypes.SuggestSuccess;
    constructor (public payload: any){}
}

export class SuggestFailure implements Action{
    readonly type = SearchActionTypes.SuggestFailure;
    constructor (public payload: any){}
}

export class FilmDetailsSuccess implements Action{
    readonly type = SearchActionTypes.FilmDetailsSuccess;
    constructor (public payload: any){}
}

export class FilmDetailsFailure implements Action{
    readonly type = SearchActionTypes.FilmDetailsFailure;
    constructor (public payload: any){}
}
export class PersistDetails implements Action{
    readonly type = SearchActionTypes.PersistDetails;
    constructor (public payload: any){}
}

export type SearchActions = LoadSearches | SuggestSearches | LoadFilmDetails | SearchSuccess | SearchFailure | SuggestSuccess | SuggestFailure | FilmDetailsSuccess | FilmDetailsFailure | PersistDetails;